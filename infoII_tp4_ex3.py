class File:
    def __init__(self):
        self.items = []

    def enqueue(self, person):
        # Si il y a déjà des éléments dans la file
        if not self.is_empty() and person.priority != 0:
            for item in self.items:
                # Si la priorité de la personne dans la file est inférieure à celle qu'on veut ajouter
                # De cette façcon, ils seront placé juste devant la première personne avec une priorité inférieure à eux
                # soit derrière toutes les priorité égales arrivées avant eux.
                if item.priority < person.priority:
                    # On l'insère dans la file à cette position
                    self.items.insert(self.items.index(item), person)
                    break
        # Si la personne à une priorité à 0, on la met à la fin
        # On obtient l'index grace à la taille de la file
        elif not self.is_empty() and person.priority == 0:
            self.items.insert(self.size(), person)
        # Sinon i la taille de la file est 0 (elle est alors vide), on insère la personne au tout début
        else :
            self.items.insert(0, person)

    def dequeue(self):
        # On met pop(0) car la personne avec la plus haute priorité se trouvera forcément ici
        return self.items.pop(0)
    
    def clear(self):
        # Tant que la file n'est pas vide, on continue de la vider
        while not self.is_empty():
            print(f"On sort ->{self.items[0].name}, priorité : {self.items[0].priority}")
            self.dequeue()

    def is_empty(self):
        return (self.items == [])
    
    def size(self):
        return len(self.items)

    def printData(self):
        for item in self.items:
            print(item.name)

class Person:
    def __init__(self):
        self.priority = None
        self.name = None

    def setPerson(self, priority, name):
        self.priority = priority
        self.name = name
    
    def getPriority(self):
        return self.priority
    
    def getName(self):
        return self.name

f = File()
ps = [(0, "Mark"), (2, "Luis"), (1, "Anna"), (3, "Sophia"), (1, "John"), (2, "Emma"), (8, "Rafael"), (6, "Theo"), (2, "Giullia"), (0, "Imane"), (2, "Sinaly")]

# On remplit la file
for p in ps:
    person = Person()
    person.setPerson(p[0], p[1])
    f.enqueue(person)